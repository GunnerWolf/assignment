const request = require('superagent');
const config = require('./config');

module.exports = {
    vehicle: vehicle
};

async function vehicle(year, manufacturer, model, rating = false) {
    let url = config.api.vehicle
        .replace(/(\\\\)*\${YEAR}/, '$1' + year)
        .replace(/(\\\\)*\${MANUFACTURER}/, '$1' + manufacturer)
        .replace(/(\\\\)*\${MODEL}/, '$1' + model);

    let response = await new Promise((resolve, reject) => {
        request
            .get(url)
            .type('json')
            .then((res) => {
                let response = { Count: res.body.Count, Results: [] };
                res.body.Results.forEach(result => {
                    response.Results.push({ Description: result.VehicleDescription, VehicleId: result.VehicleId });
                });
                resolve(response);
            }, (err) => {
                resolve({Count: 0, Results: []});
            });
    });

    if (rating && response.Results.length > 0) {
        for(let i = 0; i < response.Results.length; i++) {
            let crashRating = await getRating(response.Results[i].VehicleId);
            response.Results[i].CrashRating = crashRating;
        }
    }

    return response;
}

function getRating(vehicleId) {
    let url = config.api.rating
        .replace(/(\\\\)*\${ID}/, '$1' + vehicleId);

    return new Promise((resolve, reject) => {
        request
            .get(url)
            .type('json')
            .then((res) => {
                if (res.body.Count > 0) {
                    resolve(res.body.Results[0].OverallRating);
                } else {
                    resolve('Not Rated');
                }
            }, (err) => {
                resolve('Not Rated');
            });
    })
}