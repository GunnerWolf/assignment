const testReq = require('supertest');
const expect = require('chai').expect;
const api = require('../api');

describe('express server', () => {
    before(() => {
        // Set up test data
        testYear = 2015;
        testManufacturer = 'Audi';
        testModel = 'A3';
    });
    beforeEach(() => {
        // Init server before each test
        server = require('../server');
    });
    afterEach(() => {
        // Clean up server after each test
        server.close();
    });

    describe('vehicles endpoint', () =>{
        it('should always return json', (done) => {
            testReq(server)
                .get('/vehicles/1/x/x')
                .expect('Content-Type', /application\/json/)
                .expect(200, done);
        });
        it('should return no data when given invalid input', (done) => {
            testReq(server)
                .get('/vehicles/notARealYear/FakeManufacturer/FakeCar')
                // tell superagent to accept (and serialize) json data
                .type('json')
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res.body.Count).to.equal(0);
                    expect(res.body.Results).to.have.lengthOf(0);
                    return done();
                });
        });
        it('should return correct data when given a year, manufacturer and model', (done) => {
            testReq(server)
                .get(`/vehicles/${testYear}/${testManufacturer}/${testModel}`)
                // tell superagent to accept (and serialize) json data
                .type('json')
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res.body.Count).to.be.above(0);
                    expect(res.body.Results).to.have.lengthOf(res.body.Count);
                    res.body.Results.forEach(result => {
                        expect(result).to.have.property('Description');
                        expect(result).to.have.property('VehicleId');
                    });
                    return done();
                });
        });
        it('should return crash ratings when asked to', (done) => {
            testReq(server)
                .get(`/vehicles/${testYear}/${testManufacturer}/${testModel}`)
                .query('withRating=true')
                .type('json')
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res.body.Count).to.be.above(0);
                    expect(res.body.Results).to.have.lengthOf(res.body.Count);
                    res.body.Results.forEach(result => {
                        expect(result).to.have.property('CrashRating');
                    });
                    return done();
                });
        });
        it('should not return crash ratings when asked not to', (done) => {
            testReq(server)
                .get(`/vehicles/${testYear}/${testManufacturer}/${testModel}`)
                .query('withRating=false')
                .type('json')
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res.body.Count).to.be.above(0);
                    expect(res.body.Results).to.have.lengthOf(res.body.Count);
                    res.body.Results.forEach(result => {
                        expect(result).to.not.have.property('CrashRating');
                    });
                    return done();
                });
        });
        it('should return no results when invalid data is given and crash ratings are requested', (done) => {
            testReq(server)
                .get(`/vehicles/notARealYear/FakeManufacturer/FakeCar`)
                .query('withRating=true')
                .type('json')
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res.body.Count).to.equal(0);
                    expect(res.body.Results).to.have.lengthOf(res.body.Count);
                    return done();
                });
        });
        it('should interpret any withRating value other than "true" as false', (done) => {
            testReq(server)
                .get(`/vehicles/${testYear}/${testManufacturer}/${testModel}`)
                .query('withRating=banana')
                .type('json')
                .expect('Content-Type', /application\/json/)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res.body.Count).to.be.above(0);
                    expect(res.body.Results).to.have.lengthOf(res.body.Count);
                    res.body.Results.forEach(result => {
                        expect(result).to.not.have.property('CrashRating');
                    });
                    return done();
                });
        });
        it('should accept POST requests', (done) => {
            testReq(server)
                .post(`/vehicles`)
                .expect(200, done);
        });
        it('should not allow GET requests without parameters', (done) => {
            testReq(server)
                .get(`/vehicles`)
                .expect(404, done);
        });
        it('should return no results to an empty POST request', (done) => {
            testReq(server)
                .post(`/vehicles`)
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res.body.Count).to.equal(0);
                    expect(res.body.Results).to.have.lengthOf(res.body.Count);
                    return done();
                });
        });
        it('should return no results to an invalid POST request', (done) => {
            testReq(server)
                .post(`/vehicles`)
                .send({ wrongField: testYear, wrongName: testManufacturer, wrongJson: testModel })
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res.body.Count).to.equal(0);
                    expect(res.body.Results).to.have.lengthOf(res.body.Count);
                    return done();
                });
        });
        it('should return no results to invalid POST data', (done) => {
            testReq(server)
                .post(`/vehicles`)
                .send({ modelYear: 'notARealYear', manufacturer: 'FakeManufacturer', model: 'FakeModel' })
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res.body.Count).to.equal(0);
                    expect(res.body.Results).to.have.lengthOf(res.body.Count);
                    return done();
                });
        });
        it('should return results when given valid POST data', (done) => {
            testReq(server)
                .post(`/vehicles`)
                .send({ modelYear: testYear, manufacturer: testManufacturer, model: testModel })
                .expect(200)
                .end((err, res) => {
                    if (err) return done(err);
                    expect(res.body.Count).to.be.above(0);
                    expect(res.body.Results).to.have.lengthOf(res.body.Count);
                    res.body.Results.forEach(result => {
                        expect(result).to.have.property('Description');
                        expect(result).to.have.property('VehicleId');
                    });
                    return done();
                });
        });
    });
})