const request = require('superagent');
const testReq = require('supertest');
const expect = require('chai').expect;
const api = require('../api');

describe('api module', () => {
    describe('vehicle api', () => {
        before(() => {
            // Set up test data
            testYear = 2015;
            testManufacturer = 'Audi';
            testModel = 'A3';
        });
        it('should return deserialized JSON from the API', (done) => {
            api.vehicle(1, 'x', 'x').then(result => {
                expect(result).to.be.an('object');
                expect(result).to.have.property('Count');
                expect(result).to.have.property('Results');
            }).then(done, (err) => done(Error(err)));
        });
        it('should return a blank result when an error is returned by the API', (done) => {
            api.vehicle('null', 'x', 'x').then(result => {
                expect(result).to.deep.equal({Count: 0, Results: []});
            }).then(done, (err) => done(Error(err)));
        });
        it('should return results when valid input is given', (done) => {
            api.vehicle(testYear, testManufacturer, testModel).then(result => {
                expect(result).to.have.property('Count');
                expect(result).to.have.property('Results');
                expect(result.Count).to.be.above(0);
                expect(result.Results).to.have.lengthOf(result.Count);
            }).then(done, (err) => done(Error(err)));
        });
        it('should return crash ratings when asked', (done) => {
            api.vehicle(testYear, testManufacturer, testModel, true).then(result => {
                expect(result.Count).to.be.above(0);
                expect(result.Results).to.have.lengthOf(result.Count);
                expect(result.Results[0]).to.have.property('CrashRating');
            }).then(done, (err) => done(Error(err)));
        });
        it('should return no results when given invalid input and asked for ratings', (done) => {
            api.vehicle(1, 'x', 'x', true).then(result => {
                expect(result.Count).to.equal(0);
                expect(result.Results).to.have.lengthOf(result.Count);
            }).then(done, (err) => done(Error(err)));
        });
    })
});