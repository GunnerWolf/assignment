const express = require('express');
const app = express();

const api = require('./api');
const config = require('./config');

app.use(express.json());

app.get('/vehicles/:year/:manufacturer/:model', (req, res) => {
    let getRatings = (req.query.withRating != undefined && req.query.withRating === 'true');
    api.vehicle(req.params.year, req.params.manufacturer, req.params.model, getRatings).then((response) => {
        res.json(response);
    }, (err) => {
        res.json(err);
    });
});
app.post('/vehicles', (req, res) => {
    if (req.body && req.body.modelYear && req.body.manufacturer && req.body.model) {
        api.vehicle(req.body.modelYear, req.body.manufacturer, req.body.model).then((response) => {
            res.json(response);
        }, (err) => {
            res.json(err);
        });
    } else {
        res.json({ Count: 0, Results: [] });
    }
});

var server = app.listen(config.port, () => console.log('Server now listening on port', config.port));
module.exports = server;